package com.thinkPro.highLevel;

import org.apache.ibatis.session.SqlSession;

import com.thinkPro.domain.TrainPlanItem;
import com.thinkPro.util.MyBatisUtil;

public class TrainPlanItemCRUD {

	public boolean AddTrainPlanItem(TrainPlanItem planitem) {
		boolean result = false;
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		String statement = "com.thinkPro.mapping.trainPlanItemMapper.addTrainPlanItem";
		int temp = sqlSession.insert(statement, planitem);
		if(temp > -1){
			result = true;
		}
		sqlSession.close();
		return result;
	}

	public void DeleteTrainPlanItem(String trainPlanItemId){
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		String statement = "com.thinkPro.mapping.trainPlanItemMapper.deleteTrainPlanItem";
		int result = sqlSession.delete(statement, trainPlanItemId);
		sqlSession.close();
		System.out.println(result);
	}
	

	public boolean UpdateTrainPlanItem(TrainPlanItem planitem) {
		boolean result = false;
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		String statement = "com.thinkPro.mapping.trainPlanItemMapper.updateTrainPlanItem";
		int temp = sqlSession.insert(statement, planitem);
		if(temp > -1){
			result = true;
		}
		sqlSession.close();
		return result;
	}
}
