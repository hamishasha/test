package com.thinkPro.highLevel;

import java.util.List;

import com.thinkPro.domain.TrainPlanInfo;
import com.thinkPro.domain.TrainPlanItem;

public class TrainPlanCRUD {

	TrainPlanInfoCRUD infoCRUD = new TrainPlanInfoCRUD();
	TrainPlanItemCRUD itemCRUD = new TrainPlanItemCRUD();
	
	public boolean addTrainPlan(TrainPlanInfo planinfo,List<TrainPlanItem> planitems){
		boolean result = false;
		if(infoCRUD.AddTrainPlanInfo(planinfo)){
			result = true;
		}
		for(TrainPlanItem item:planitems){
			if(itemCRUD.AddTrainPlanItem(item)){
				result = true;
			}else{
				break;
			}
		}
		return result;
	}
	
	public boolean deleteTrainPlan(String trainPlanInfoId){
		boolean result  = false;
		if(infoCRUD.DeleteTrainPlanInfo(trainPlanInfoId)){
			result = true;
		}
		return result;
	}

}
