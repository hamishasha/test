package com.thinkPro.highLevel;

import org.apache.ibatis.session.SqlSession;

import com.thinkPro.domain.TrainPlanInfo;
import com.thinkPro.util.MyBatisUtil;



public class TrainPlanInfoCRUD {

	public boolean AddTrainPlanInfo(TrainPlanInfo planinfo) {
		boolean result = false;
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		String statement = "com.thinkPro.mapping.trainPlanInfoMapper.addTrainPlanInfo";
		int temp = sqlSession.insert(statement, planinfo);
		if(temp > -1){
			result = true;
		}
		sqlSession.close();
		return result;
	}
	
	public boolean DeleteTrainPlanInfo(String trainPlanInfoId){
		boolean result = false;
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		String statement = "com.thinkPro.mapping.trainPlanInfoMapper.deleteTrainPlanInfo";
		int temp = sqlSession.insert(statement, trainPlanInfoId);
		if(temp > -1){
			result = true;
		}
		sqlSession.close();
		return result;
	}
	
	public boolean UpdateTrainPlanInfo(TrainPlanInfo planinfo){
		boolean result = false;
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		String statement = "com.thinkPro.mapping.trainPlanInfoMapper.updateTrainPlanInfo";
		int temp = sqlSession.insert(statement, planinfo);
		if(temp > -1){
			result = true;
		}
		sqlSession.close();
		return result;
	}
}
