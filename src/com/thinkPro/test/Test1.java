package com.thinkPro.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.thinkPro.domain.TrainPlanInfo;
import com.thinkPro.highLevel.TrainPlanInfoCRUD;

public class Test1 {

	public static void main(String[] args) throws IOException {
	
		TrainPlanInfo planInfo = new TrainPlanInfo();
		planInfo.setTRAIN_PLAN_ID("005");
		planInfo.setTRAIN_PLAN_NAME("数据库设计1");
		planInfo.setTRAIN_PLAN_TYPE("员工管理1");
		planInfo.setTRAIN_PLAN_YEAR("2011");
		planInfo.setSTART_TIME(new Date("2016/04/09"));
		planInfo.setEND_TIME(new Date("2016/09/09"));
		planInfo.setIS_FINISH(0);
		// 执行插入操作
		
		TrainPlanInfoCRUD tpi= new TrainPlanInfoCRUD();
		//Boolean result = tpi.AddTrainPlanInfo(planInfo);
		
		Boolean result = tpi.UpdateTrainPlanInfo(planInfo);
		System.out.println(result);
		
	}
}