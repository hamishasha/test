package com.thinkPro.test;

import java.util.Date;
import java.util.List;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import com.thinkPro.domain.TrainPlanInfo;
import com.thinkPro.util.MyBatisUtil;

public class TestPlanMapper {

	@Test
	public void testAddPlan() {
		
		// SqlSession sqlSession = MyBatisUtil.getSqlSession(false);
		
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		
		String statement = "com.thinkPro.mapping.trainPlanInfoMapper.addTrainPlanInfo";// 映射sql的标识字符串
		
		TrainPlanInfo plan = new TrainPlanInfo();
		
		plan.setTRAIN_PLAN_ID("x");
		plan.setTRAIN_PLAN_NAME("数据库设计");
		plan.setTRAIN_PLAN_TYPE("员工管理");
		plan.setTRAIN_PLAN_YEAR("2010");
		plan.setSTART_TIME(new Date("2016/04/09"));
		plan.setEND_TIME(new Date("2016/09/09"));
		plan.setIS_FINISH(0);
		// 执行插入操作
		
		int retResult = sqlSession.insert(statement, plan);
		
		sqlSession.close();
		
		System.out.println(retResult);
	}

	@Test
	public void testUpdate() {
		
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);
		
	
		String statement = "com.thinkPro.mapping.trainPlanInfoMapper.updateTrainPlanInfo";// 映射sql的标识字符串
		
		TrainPlanInfo plan = new TrainPlanInfo();
		
		plan.setTRAIN_PLAN_ID("x");
		plan.setTRAIN_PLAN_NAME("数据库设计1");
		plan.setTRAIN_PLAN_TYPE("员工管理1");
		plan.setTRAIN_PLAN_YEAR("2011");
		plan.setSTART_TIME(new Date("2016/05/09"));
		plan.setEND_TIME(new Date("2016/20/09"));
		plan.setIS_FINISH(0);
		
		// 执行修改操作
		
		int retResult = sqlSession.update(statement, plan);
		
		// 使用SqlSession执行完SQL之后需要关闭SqlSession
		
		sqlSession.close();
		
		System.out.println(retResult);
	}

	@Test
	public void testDelete() {
		
		SqlSession sqlSession = MyBatisUtil.getSqlSession(true);

		String statement = "com.thinkPro.mapping.trainPlanInfoMapper.deleteTrainPlanInfo";// 映射sql的标识字符串
		// 执行删除操作
		
		int retResult = sqlSession.delete(statement, "005");
		int retResult1 = sqlSession.delete(statement, "001");
		// 使用SqlSession执行完SQL之后需要关闭SqlSession
		
		sqlSession.close();
		
		System.out.println(retResult);
		System.out.println(retResult1);
	}

	
}